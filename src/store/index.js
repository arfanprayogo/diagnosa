import { createStore } from 'vuex'

export default createStore({
  state: {
    app: {
      name : "Admin"
    }
  },
  getters: {
  },
  mutations: {
  },
  actions: {
  },
  modules: {
  }
})
