import { createRouter, createWebHistory } from 'vue-router'
import MainLayout from '../layouts/MainLayout.vue'
import HomeView from '../views/HomeView.vue'

const routes = [
  { 
    path: '/',
    component: MainLayout,
    children: [
      {
        path: '',
        name: 'home',
        component: HomeView
      },
      {
        path: 'about',
        name: 'about',
        component: function () {
          return import('../views/AboutView.vue')
        }
      },
      {
        path: 'pasien',
        name: 'pasien',
        component: function () {
          return import('../views/PasienView.vue')
        }
      },
      {
        path: 'diagnosis',
        name: 'diagnosis',
        component: function () {
          return import('../views/DiagnosisView.vue')
        }
      },
      {
        path: 'faskes',
        name: 'faskes',
        component: function () {
          return import('../views/FaskesView.vue')
        }
      },
        {
          path: 'setting',
          name: 'setting',
          component: function () {
            return import('../views/SettingView.vue')
          }
      }
    ]
  },

]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
